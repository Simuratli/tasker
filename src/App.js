import {useState,useContext,useEffect} from 'react'
import Column from './column'
import {DragDropContext} from 'react-beautiful-dnd'
import styled from 'styled-components'
import {DragContext} from './context/DragDropContext'
import './style/main.css'
import Sidebar from './components/Sidebar/Sidebar'
import {Route,Switch} from 'react-router-dom'
import Login from './pages/Login/Login'
import Register from './pages/Register/Register'

function Tasker({history}) {
  const  [dragDrop, setDragDrop] = useContext(DragContext)
  console.log("User in app ", dragDrop.user);
  const Container = styled.div`
    width:100%;
    min-height:100vh;
    display:flex;
    flex-direction:row;
  `;


  useEffect(() => {
      if(!dragDrop.user){
        history.push('/login')
      }
  }, [dragDrop.user,history])

  const [state, setState] = useState(null)

  useEffect(() => {
    setState(dragDrop)
  }, [dragDrop])



  function onDragEnd(result) {  
    const {destination,source,draggableId} = result
    if(!destination){
      return
    }
    if(destination.droppableId === source.droppableId && 
      destination.index  === source.index){
        return;
      } 
  
      const start  = state.columns[source.droppableId];
      const finish  = state.columns[destination.droppableId];

      if(start===finish){
        const newTaskIds = Array.from(start.taskIds)
        newTaskIds.splice(source.index,1)
        newTaskIds.splice(destination.index,0,draggableId)
        const newColumn = {
          ...start,
          taskIds:newTaskIds
        }
  
        const newState = {
          ...state,
          columns:{
            ...state.columns,
            [newColumn.id]:newColumn
          }
        }

        setDragDrop(newState)
        localStorage.setItem('drag',JSON.stringify(newState))
        return; 
      }

      // Moving from one list to another
      const startTaskIds = Array.from(start.taskIds)
      startTaskIds.splice(source.index,1)
      const newStart = {
        ...start,
        taskIds:startTaskIds
      }
      const finishTaskIds = Array.from(finish.taskIds)
      finishTaskIds.splice(destination.index , 0 , draggableId);
      const newFinish = {
        ...finish,
        taskIds:finishTaskIds
      }

      const newState = {
        ...state,
        columns:{
          ...state.columns,
          [newStart.id]:newStart,
          [newFinish.id]:newFinish,

        }
      }
      setDragDrop(newState)
      localStorage.setItem('drag',JSON.stringify(newState))
      document.body.style.color = 'black'
  }

  function onDragStart() {
    document.body.style.color = 'orange'
  }

  function onDragUpdate(update){
    const {destination} = update
    const opacity = destination
    ? destination.index / Object.keys(state.tasks).length : 
    0;
    document.body.style.backgroundColor = `rgba(153,141,217,${opacity})`
  }

  return (
    state && <DragDropContext onDragEnd={onDragEnd} onDragUpdate={onDragUpdate} onDragStart={onDragStart}>
      
      <Container>
        <Sidebar/>
        <div className='tasker-scroll'>
          {state.columnOrder && state.columnOrder.map((columnId)=>{
            const column = state.columns[columnId]
            const task = column.taskIds.map(taskId=>state.tasks[taskId])
            return <Column key={column.id} column={column} tasks={task} />
          })}
        </div>
      </Container>

    </DragDropContext>
  );
}

function App() {
  const  [dragDrop, setDragDrop] = useContext(DragContext)

  return(
    <Switch>
      <Route path='/' exact component={Tasker} />
      <Route path='/login' exact component={Login} />
      <Route path='/register' exact component={Register} />
    </Switch>
  )
}
  


export default App;
