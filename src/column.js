import React from 'react'
import styled from 'styled-components'
import Task from './task'
import {Droppable} from 'react-beautiful-dnd'
import sun from './assets/sun.png'
import cloud from './assets/cloud.png'
import snow from './assets/snow.png'



const TaskList = styled.div`
    padding:8px;
    min-height:100vh;
    flex-grow:1;'                                                                                           '
    transition:0.4s;
    background:${props=>(props.isDraggingOver && 'skyblue')}
`;


function Column(props) {
    return (
        <div style={{background:props.column.id === 'column-1' ? '#FFE1E1' : 
        props.column.id === 'column-2' ? '#F1FDC0' : 
        props.column.id === 'column-3' ? '#CAF3E9' : ''
    }} className='columns-container'>
            <div className='column-head'>
            <img
                src={
                    props.column.id === 'column-1' ? snow : 
                    props.column.id === 'column-2' ? cloud : 
                    props.column.id === 'column-3' ? sun  : ''
                }
                alt='list icons'
                />
            <h1 className='column-title'>
                {props.column.title}
            </h1>
            </div>
            <Droppable  droppableId={props.column.id}>
                {(provided,snapshot)=>(
                    <TaskList
                    className='taks-list'
                        isDraggingOver = {snapshot.isDraggingOver}
                        ref={provided.innerRef}     
                        {...provided.droppableProps}>
                            {props.tasks.map((task,index)=>{
                                return <Task key={task.id} index={index} task={task} />
                            })}
                            {provided.placeholder}
                        </TaskList>
                )}
                
            </Droppable>
        </div>
    )
}

export default Column
