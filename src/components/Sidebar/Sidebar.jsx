import React,{useContext} from 'react'
import './Sidebar.css'
import {Link} from 'react-router-dom'
import settings from '../../assets/settings.png'
import plus from '../../assets/plus.svg'
import {DragContext} from '../../context/DragDropContext'
import app from '../../firebase'
import {useHistory} from 'react-router-dom'

function Sidebar() {
    const history = useHistory()
    const  [dragDrop, setDragDrop] = useContext(DragContext)

    function addNewCard(){
        dragDrop.addNewCard()
    }

    return (
        <div className='sidebar'>
            <h1>Tasker</h1>
            <button onClick={()=>{addNewCard()}} className="addTask">
                <img alt='plus' src={plus}  />
            </button>

            <button onClick={async ()=>{await app.auth().signOut(); window.location.reload(); history.push('/login')}} className='menu'>
                <img alt='settings' src={settings} />
            </button>
        </div>
    )
}

export default Sidebar
