import React,{useContext,useRef} from 'react'
import './Modal.css'
import {DragContext} from '../../context/DragDropContext'

import x from '../../assets/x.svg'

function Modal(props) {
    const context = useContext(DragContext)
    let tagRef = useRef(null)
    let headRef = useRef(null)
    let descriptionRef = useRef(null)

    


    function handleSubmit() {  
        let tag = tagRef.current.textContent
        let head = headRef.current.textContent
        let description = descriptionRef.current.textContent
        context[0].setTask(props.task.id, tag,head,description)
    }
    
    
    function closeModal(){
        props.setModal(false)
        handleSubmit()
     }
    

    return (
        <>
        <div className={`backdrop ${props.open && "open" }`} onClick={closeModal}>
        </div>
        <div className={`modal ${props.open && "open" }`}>
        <div className='card'>
            <span ref={tagRef} suppressContentEditableWarning={true}  contentEditable className='card-head'>{props.task.content.tag}</span>
            <h1 ref={headRef} contentEditable suppressContentEditableWarning={true} className='card-header'>{props.task.content.head}</h1>
            <p ref={descriptionRef} contentEditable suppressContentEditableWarning={true} className='card-description'>{props.task.content.description}</p>
            <hr/>
            <p className='card-date'>Last Update <span className='date'>{props.task.content.date}</span></p>
        </div>
            <button className='closeButton' onClick={handleSubmit}>
                <img alt='close button' src={x} />
            </button>
        </div>
        </>
    )
}

export default Modal
