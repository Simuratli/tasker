
import React,{useState} from 'react'
import styled from 'styled-components'
import {Draggable} from 'react-beautiful-dnd'
import Modal from './components/Modal/Modal'

function Task(props) {

    const [modal, setModal] = useState(false)

    function openModal(){
        setModal(!modal)
    }

    const Container = styled.div`
        padding:8px;
        margin-bottom:8px;
        border-radius:2px;
        transition:0.4s;
        background:${props => props.isDragging ? 'lightgreen' : 'transparent'} 
    `;
    
    return (
       <>
        <Modal task={props.task} content={props.task.content} open={modal} setModal={()=>{setModal()}}/>
        <Draggable  index={props.index} draggableId={props.task.id}>
            {(provided,snapshot)=>(<Container
                {...provided.draggableProps}
                {...provided.dragHandleProps}
                ref={provided.innerRef}
                isDragging={snapshot.isDragging}
                onClick={openModal}
                >
                    <div className='card'>
                        <span className='card-head'>{props.task.content.tag}</span>
                        <h1 className='card-header'>{props.task.content.head}</h1>
                        <p className='card-description'>{props.task.content.description}</p>
                        <hr/>
                        <p className='card-date'>Last Update <span className='date'>{props.task.content.date}</span></p>
                    </div>
                </Container>)}
        </Draggable>
       </>
    )
}

export default Task
