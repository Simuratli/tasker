import React,{useEffect,useState,useContext} from 'react'
import './Login.css'
import {DragContext} from '../../context/DragDropContext'
import {toast} from 'react-toastify'
import app from '../../firebase'
import {Link} from 'react-router-dom'
import firebase from 'firebase'
var provider = new firebase.auth.GoogleAuthProvider();
function Login({history}) {
    const [dragDrop, setDragDrop] = useContext(DragContext)
    const [password, setPassword] = useState('')
    const [email, setEmail] = useState('')

    useEffect(() => {
        let emailStore = localStorage.getItem('taskerEmailLogin')
        setEmail(emailStore)
    }, [])

    useEffect(() => {
        if(dragDrop.user){
          history.push('/')
        }
    }, [])

    async function gmailReg(){
        try {
            const result = await firebase.auth().signInWithPopup(provider)
            console.log(result.user);
            setDragDrop((prev)=>({
                ...prev,
                user:result.user
            }))
            console.log(result.user.email);
            
            history.push('/')
            toast.success("Succesfully login")
        } catch (error) {
            console.log(error);
        }
    }

    async function handleLogin(e){
        e.preventDefault()
        try {
            const result = await app.auth().signInWithEmailAndPassword(email,password)
            await result.user.getIdToken()
            console.log(result.user.email);
            dragDrop.setCurrentUser(app.auth().currentUser) 
            setDragDrop((prev)=>({
                ...prev,
                user:app.auth().currentUser
            }))
            toast.success("Succesfully login")
            history.push('/')
        } catch (error) {
            toast.error(error.message)
        }


    }

    return (
        <div className='login-container'>
            <div className='login'>
                <form onSubmit={handleLogin}>
                    <h1>Login</h1>
                    <input value={email} onChange={(e)=>{setEmail(e.target.value)}} type='email' placeholder='Email' />
                    <input onChange={(e)=>{setPassword(e.target.value)}} type='password' placeholder='Password' />
                    <button className='login-button'>Login</button>
                    <button  onClick={gmailReg} className='google-button'>Login with Google</button>
                    <Link to='/register'>You do not have account</Link>
                </form>
            </div>
        </div>
    )
}

export default Login
