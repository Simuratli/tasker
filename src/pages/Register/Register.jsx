import React,{useEffect,useState,useContext} from 'react'
import app from '../../firebase'
import firebase from 'firebase'
import {toast} from 'react-toastify'
import {Link} from 'react-router-dom'
import {DragContext} from '../../context/DragDropContext'
var provider = new firebase.auth.GoogleAuthProvider();
function Register({history}) {
    const [password, setPassword] = useState('')
    const [email, setEmail] = useState('')
    const [dragDrop,setDragDrop] = useContext(DragContext)
    useEffect(() => {
        if(dragDrop.user){
          history.push('/')
        }
    }, [dragDrop.user,history])

    async function registerFirebase(e){
        e.preventDefault()
        try {
            await app.auth().createUserWithEmailAndPassword(email,password)
            localStorage.setItem("taskerEmailLogin",email)
            history.push('/login')
            toast.success("Succesfully login")
        } catch (error) {
            toast.success(error.message)
        } 
    }

    async function gmailReg(){
        try {
            const result = await firebase.auth().signInWithPopup(provider)
            
            setDragDrop((prev)=>({
                ...prev,
                user:result.user
            }))
            history.push('/')
            toast.success("Succesfully login")
        } catch (error) {
            console.log(error);
        }
    }

    return (
        <div className='login-container'>
            <div className='login'>
                <form onSubmit={registerFirebase}>
                    <h1>Register</h1>
                    <input onChange={(e)=>{setEmail(e.target.value)}} type='email' placeholder='Email' />
                    <input onChange={(e)=>{setPassword(e.target.value)}} type='password' placeholder='Password' />
                    <button className='login-button'>Register</button>
                    <button type='button' onClick={gmailReg} className='google-button'>Register with Google</button>
                    <Link to='/login'>You  have account</Link>
                </form>
            </div>
        </div>
    )
}

export default Register
