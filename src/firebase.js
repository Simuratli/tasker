import firebase from 'firebase'
import 'firebase/auth'

const app = firebase.initializeApp({
    apiKey: "AIzaSyC6J5hAGAajwaDWPPLMNkD8aUl56NFlwwc",
    authDomain: "task-manager-f7b8d.firebaseapp.com",
    projectId: "task-manager-f7b8d",
    storageBucket: "task-manager-f7b8d.appspot.com",
    messagingSenderId: "479396488459",
    appId: "1:479396488459:web:44d89cf7575006eaf4ae0e"
})

export default app