import React,{useState,createContext} from 'react'
export const DragContext = createContext([1,23,54,23])



export const DragDropProvider = props =>{
    
    

    const [user, setUser] = useState(null)
    
    function setCurrentUser(e){
        setUser(e)
        console.log("Context user ",e);
    }

    function setTask(id,tag,head,description) {  
        setDragDrop((prevValue)=>({
            ...prevValue,
            tasks:{
                ...prevValue.tasks,
                [id]:{
                    id:id,
                    content:{
                        tag:tag,
                        head:head,
                        description:description,
                        date:new Date().getMonth() + "/" + new Date().getDay() + "/" + new Date().getFullYear()
                    }
                }
            }
        }))
    }

    async function addNewCard() {
        const length = await Math.random()
        let number = Number(length) * 100
        const name = await "task-"+number;
        
        await setDragDrop((prev)=>({
            ...prev,
            tasks:{
                ...prev.tasks,
                [name]:{
                    id:name,
                    content:{
                        tag:'New Tag',
                        head:"New Heading",
                        description:"New Description",
                        date:new Date().getMonth() + "/" + new Date().getDay() + "/" + new Date().getFullYear()
                    }
                }

            },
            columns:{
                ...prev.columns,
                'column-1':{
                    ...prev.columns['column-1'],
                    taskIds:[...prev.columns['column-1'].taskIds, name]
                },
            }
        }))

        await localStorage.setItem('dragData',JSON.stringify(dragDrop))
    }

    const [dragDrop, setDragDrop] = useState({
        tasks:{
            'task-1':{
                id:"task-1",
                content:{
                    tag:'deneme',
                    head:"Garbage Heading",
                    description:"Charge phone",
                    date:new Date().getMonth() + "/" + new Date().getDay() + "/" + new Date().getFullYear()
                }
            },
            'task-2':{
                id:"task-2",
                content:{
                    tag:'deneme',
                    head:"Watch Heading",
                    description:"Charge phone",
                    date:new Date().getMonth() + "/" + new Date().getDay() + "/" + new Date().getFullYear()
                }
            },
            'task-3':{
                id:"task-3",
                content:{
                    tag:'deneme',
                    head:"PAY ATONAN Heading",
                    description:"Charge phone",
                    date:new Date().getMonth() + "/" + new Date().getDay() + "/" + new Date().getFullYear()
                }
            },
        },
        columns:{
            'column-1':{
                id:'column-1',
                title:"To do",
                taskIds:['task-1','task-2','task-3']
            },
            'column-2':{
                id:'column-2',
                title:"In Progress",
                taskIds:[]
            },
            'column-3':{
                id:'column-3',
                title:"Done",
                taskIds:[]
            },
        },
        columnOrder:['column-1','column-2','column-3'],
        setTask:setTask,
        addNewCard:addNewCard,
        user:user,
        setCurrentUser:setCurrentUser
    })

    return <DragContext.Provider value={[dragDrop, setDragDrop]}>
        {props.children}
    </DragContext.Provider>
}


